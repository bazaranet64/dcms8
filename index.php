<?php
version_compare(PHP_VERSION, '5.3', '>=') or die('Требуется PHP >= 5.3');

if (@function_exists('ini_set')) {
    // игнорировать повторяющиеся ошибки
    ini_set('ignore_repeated_errors', true);

    // показываем только фатальные ошибки
    ini_set('error_reporting', E_ERROR);

    // непосредственно, включаем показ ошибок
    ini_set('display_errors', true);
}

define('ROUTE_PROP_NAME', 'DCMS_ROUTE');

/**
 * @const H путь к корневой директории сайта
 */
define('H', __DIR__);

/**
 * @const TIME_START Время запуска скрипта в миллисекундах
 */
define('TIME_START', microtime(true)); // время запуска скрипта

/**
 * @const IS_WINDOWS Запущено ли на винде
 */
if (!defined('IS_WINDOWS')) {
    define('IS_WINDOWS', strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
}

// устанавливаем Московскую временную зону по умолчанию
if (@function_exists('ini_set')) {
    ini_set('date.timezone', 'Europe/Moscow');
}

/**
 * @const TMP временная папка
 */
define('TMP', H.'/sys/tmp');
/**
 * @const TIME UNIXTIMESTAMP
 */
define('TIME', time());
/**
 * @const THEMES_PATH Путь к папке с темами оформления
 */
define('THEMES_PATH', H.'/sys/themes');
/**
 * @const SETTINGS_PATH Путь к папке с настройками
 */
define('SETTINGS_PATH_DEFAULT', H.'/sys/settings/default');
/**
 * @const SETTINGS_PATH Путь к папке с настройками
 */
define('SETTINGS_PATH', H.'/sys/settings/installed');
/**
 * @const CLASSES_PATH Путь к папке с системными классами
 */
define('CLASSES_PATH', H.'/sys/classes');
/**
 * @const MODULES_PATH Путь к папке с модулями
 */
define('MODULES_PATH', CLASSES_PATH.'/Modules');


require_once(CLASSES_PATH.'/SplClassLoader.php');
/**
 * Автоматическая загрузка классов из папки CLASSES_PATH в соответствии с PSR-0
 */
$classLoader = new SplClassLoader(null, CLASSES_PATH);
$classLoader->register();

use Dcms\Core\Modules;
use Dcms\Core\Response\Response;
use Dcms\Core\Response\ErrorException;

/**
 * @param *
 * @return string
 */
function __()
{
    $args = func_get_args();
    $args_num = count($args);
    if (!$args_num) {
        // нет ни строки ни параметров, вообще нихрена
        return '';
    }

    Modules::executeHook('__', $args[0]);

    if ($args_num == 1) {
        // строка без параметров
        return $args[0];
    }

    $args4eval = array();
    for ($i = 1; $i < $args_num; $i++) {
        $args4eval[] = '$args['.$i.']';
    }
    return eval('return sprintf($args[0],'.implode(',', $args4eval).');');
}
session_start();

try {
    Modules::executePreprocess();
    $response = Modules::executeRequest();
    Modules::executePostprocess($response);
    $response->output();
} catch (ErrorException $e) {
    // нормальное сообщение об ошибке, когда контроллер бросает исключение, что не может обработать запрос.
    $response = new Response();
    $response->page->setTitle(__("Ошибка"));
    $response->page->err($e->getMessage());
    $response->output();
} catch (\Exception $ex) {
    try {
        // сначала пробуем нормально отобразить страницу с исключением
        $response = new Response();
        $response->page->setTitle(__("Ошибка"));
        $response->page->addContentHtml('<pre>'.print_r($ex, true).'</pre>');
        $response->output();
    } catch (\Exception $e) {
        // если совсем звездец...
        header("Content-Type: text/plain");
        print_r($ex);
        print_r($e);
    }
}
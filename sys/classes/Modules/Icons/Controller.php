<?php

namespace Modules\Icons;

use Dcms\Core\Response\Response;
use Dcms\Models\Ui\Content\Items\Posts\Post;

class Controller extends \Dcms\Core\Controller
{
    protected
        $_all_icons,
        $_skipped_icons,
        $_rebuild,
        $_sprite_path_css = '/files/icons.css',
        $_sprite_path_image = '/files/icons.png';

    function __construct($module)
    {
        parent::__construct($module);

        $this->_all_icons = $this->getSettings()->get('all_icons', array());
        $this->_skipped_icons = $this->getSettings()->get('skipped_icons', array());
        $this->_rebuild = false;
    }

    protected function _rebuild()
    {
        $skipped_icons = $this->_skipped_icons;
        $checked_icons = array();
        $icons_sprite = new Sprite();

        foreach ($this->_all_icons AS $icon_path => $icon_class) {
            $abs_path = realpath(H.$icon_path);

            if (!$abs_path) {
                $skipped_icons[] = $icon_path;
                continue;
            }

            try {
                $icon_class = $icons_sprite->addImage($abs_path);
                $checked_icons[$icon_path] = $icon_class;
            } catch (\Exception $ex) {
                $skipped_icons[] = $icon_path;
            }
        }

        $icons_sprite->bindIndexes();
        $icons_sprite->saveSpriteImage($this->getCurrentPathAbs().$this->_sprite_path_image);
        $icons_sprite->saveSpriteCss($this->getCurrentPathAbs().$this->_sprite_path_css,
            $this->getCurrentPathRel().$this->_sprite_path_image);

        $this->getSettings()->set('all_icons', $checked_icons);
        $this->getSettings()->set('skipped_icons', $skipped_icons);
    }

    /**
     *
     * @param Post $post
     */
    function HOOK_replace_icon(Post $post)
    {
        $icon_src = $post->getIconSrc();

        if (!$icon_src) {
            return;
        }

        if (in_array($icon_src, $this->_skipped_icons)) {
            return;
        }

        if (array_key_exists($icon_src, $this->_all_icons)) {
            $class_name = $this->_all_icons[$icon_src];
            $post->icon_html = '<span class="icons '.$class_name.'" ></span>';
        } else {
            $this->_all_icons[$icon_src] = null;
            $this->_rebuild = true;
        }
    }

    /**
     * @param $response Response
     */
    function POSTPROCESS_icons(Response $response)
    {
        if ($this->_rebuild) {
            $this->_rebuild();
        }

        $response->page->meta->styles[] = $this->getCurrentPathRel().'/files/icons.css';
    }
}
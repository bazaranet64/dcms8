<?php

namespace Modules\Icons;

/**
 * Description of Icon
 *
 * @author pavlovd
 */
class Icon
{
    public $path;
    public $class = "";
    public $w, $h; // размеры
    public $x, $y; // позиция в спрайте
    protected $_image;

    function __construct($path)
    {
        $this->path = $path;
        $info = @getimagesize($this->path);

        if ($info === false) {
            throw new \Exception(__('Не удалось открыть иконку %s', basename($path)));
        }

        $this->w = $info[0];
        $this->h = $info[1];
    }

    function getImage()
    {
        if (!$this->_image) {
            $this->_image = imagecreatefromstring(file_get_contents($this->path));
        }
        return $this->_image;
    }
}
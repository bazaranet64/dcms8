<?php

namespace Modules\Modules;

use Dcms\Core\Response\Response;
use Dcms\Core\Request\Request;
use Dcms\Core\Modules;
use Dcms\Core\Response\Error404Exception;
use Dcms\Core\Url;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $response Response
     */
    function GET_modules(Response $response)
    {
        switch (Request::getUrl()->getParam('filter', 'all')) {
            case 'installed':
                $filter = 'installed';
                $modules = Modules::getInstalled();
                break;
            default:
                $filter = 'all';
                $modules = Modules::getAll();
                break;
        }

        $response->page->setTitle('Список модулей');
        $response->page->breadcrumbs->add(__('Админка'), '/admin/');

        $response->page->tabs->addItem(__('Все'), Request::getUrl()->setParam('filter', 'all'), $filter == 'all');
        $response->page->tabs->addItem(__('Установленные'), Request::getUrl()->setParam('filter', 'installed'),
            $filter == 'installed');

        foreach ($modules AS $module) {
            $post = $response->page->content->addPost();
            $post->title = $module->getName();
            $post->setUrl(new Url('/admin/modules/module/', array('name' => $module->getName(), 'filter' => $filter)));
        }
    }

    /**
     * @param $response Response
     * @throws Error404Exception
     */
    function GET_module(Response $response)
    {
        $module_name = Request::getUrl()->getParam('name');

        if (!$module_name) {
            throw new Error404Exception();
        }

        $module = Modules::getByName($module_name);

        if (!$module) {
            throw new Error404Exception();
        }

        $response->page->breadcrumbs->add(__('Админка'), '/admin/');
        $response->page->breadcrumbs->add(__('Модули'),
            Request::getUrl()->setPath('/admin/modules/')->removeParam('name'));


        $response->page->setTitle(__('Модуль "%s"', $module->getName()));
    }

    /**
     * Обрабатывается только для /admin/
     * @param $response Response
     */
    function POSTPROCESS_admin(Response $response)
    {
        $content = $response->page->content;

        $post = $content->addPost();
        //$post->setUrl('admin/modules/');
        $post->setTitle(__('Модули'));

        $child_post = $post->addPost();
        $child_post->setTitle(__('Список модулей'));
        $child_post->setUrl('/admin/modules/all/');

        $child_post = $post->addPost();
        $child_post->setTitle(__('Приоритет модулей'));
        $child_post->setUrl('/admin/modules/priority/controller/');
    }
}
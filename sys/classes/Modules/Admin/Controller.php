<?php

namespace Modules\Admin;

use Dcms\Core\Response\Response;

class Controller extends \Dcms\Core\Controller
{

    /**
     * Обработчик запроса /admin/
     * Ссылки на системные настройки добавляются в модулях в методах response_postprocess
     * @param $response Response
     */
    function GET_index(Response $response)
    {
        $response->page->setTitle(__('Админка'));
    }
}
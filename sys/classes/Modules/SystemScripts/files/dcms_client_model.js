(function (cm) {
    /**
     * Применение изменений к существующей модели на клиенте
     * @param {object} client_model Существующая модель
     * @param {object} modification Данные для модификации модели
     */
    cm.mergeModel = function (client_model, modification) {
        for (var key in modification) {
            if (!modification.hasOwnProperty(key))
                continue;
            var val = modification[key];

            // объект с параметрами для модификации
            if (val !== null && typeof val === "object" && val['@MODIFY']) {

                var i, index, content, from, to,
                    mod = val['@MODIFY'],
                    length = mod['LENGTH'],
                    merge = mod['MERGE'],
                    replace = mod['REPLACE'],
                    move = mod['MOVE'];

                // перемещение элементов в списке
                for (i = 0; i < move.length; i++) {
                    from = move[i]['f'];
                    to = move[i]['t'];


                    client_model[key][to] = $.extend(true, {}, client_model[key][from]);
                    client_model[key][from] = $.extend(true, {}, client_model[key][from]);
                    delete client_model[key][to]['$$hashKey'];
                    delete client_model[key][from]['$$hashKey'];
                }

                // замена(вставка) элементов в списке
                for (i = 0; i < replace.length; i++) {
                    index = replace[i]['i'];
                    content = replace[i]['c'];

                    client_model[key][index] = content;
                }

                // изменение данных у элементов списка
                for (i = 0; i < merge.length; i++) {
                    index = merge[i]['i'];
                    content = merge[i]['c'];
                    cm.mergeModel(client_model[key][index], content);
                }

                client_model[key].length = length;

            }
            else if (val !== null && typeof val === "object" && !Array.isArray(val)) {

                if (typeof client_model[key] !== "object" || !client_model[key])
                    client_model[key] = {};

                cm.mergeModel(client_model[key], val);
            }
            else {
                // скаляр
                client_model[key] = val;
                //delete client_model['$$hashKey'];
            }
        }
    };

})(window.DCMS_CLIENT_MODEL = {});



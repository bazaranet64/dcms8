<?php

namespace Modules\Notifications;

use Dcms\Models\Ui\Meta\Meta;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_notifications(Meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel().'/files/notifications.js');
    }
}
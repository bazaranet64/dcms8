<?php

namespace Modules\HighCharts;

use Dcms\Models\Ui\Meta\Meta;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_highcharts(Meta $page_meta)
    {
        $page_meta->scripts[] = $this->getCurrentPathRel().'/files/highcharts.js';
    }
}
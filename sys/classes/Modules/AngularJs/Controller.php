<?php

namespace Modules\AngularJs;

use Dcms\Models\Ui\Meta\Meta;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_angular(Meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel().'/files/angular.js');
    }

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_highcharts(Meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel().'/files/angular-highchart.js');
    }

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_pagination(Meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel().'/files/angular-pagination.js');
    }

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_elastic(Meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel().'/files/angular-elastic.js');
    }
}
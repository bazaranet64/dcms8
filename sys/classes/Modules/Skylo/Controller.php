<?php

namespace Modules\Skylo;

use Dcms\Models\Ui\Meta\Meta;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_skylo(Meta $page_meta)
    {
        $page_meta->scripts[] = $this->getCurrentPathRel().'/files/skylo.js';
        $page_meta->styles[] = $this->getCurrentPathRel().'/files/skylo.css';
    }
}
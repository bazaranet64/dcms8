<?php

namespace Modules\Cache\Engines;

use Modules\Cache\ICacheEngine;

class xCache implements ICacheEngine
{

    public function clear($key)
    {
        xcache_unset($key);
    }

    public function get($key)
    {
        return xcache_get($key);
    }

    public function set($key, $content, $ttl)
    {
        return xcache_set($key, $content, $ttl);
    }

    public static function canUsing()
    {
        return extension_loaded('xcache');
    }
}
<?php

namespace Modules\Dev;

class Controller extends \Dcms\Core\Controller
{
    protected $_hooks_called = array();

    function HOOK_all($data)
    {
        $hook_name = $data['hook_name'];
        $hook_data = &$data['hook_data'];

        $this->_hooks_called[] = $hook_name;
    }

    function __destruct()
    {
        $need_save = false;
        $hooks = $this->getSettings()->get('hooks', array());
        foreach ($this->_hooks_called AS $hook_name) {
            if (in_array($hook_name, $hooks)) continue;
            $hooks[] = $hook_name;
            $need_save = true;
        }

        if ($need_save) $this->getSettings()->set('hooks', $hooks);
    }
}
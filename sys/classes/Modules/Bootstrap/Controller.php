<?php

namespace Modules\Bootstrap;

use Dcms\Models\Ui\Meta\Meta;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_bootstrap(Meta $page_meta)
    {
        $page_meta->scripts[] = $this->getCurrentPathRel().'/files/bootstrap.js';
        $page_meta->styles[] = $this->getCurrentPathRel().'/files/bootstrap.css';
    }
}
<?php

namespace Modules\Example;

use Dcms\Core\Request\Request;
use Dcms\Core\Response\Response;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $response Response
     */
    function GET_index(Response $response)
    {
        $page = $response->page;

        $page->setTitle("Сообщение ".Request::getParam('post', 'не выбрано'));

        $page->err(__('Сообщение об ошибке'));
        $page->msg(__('Информативное сообщение'));

        $menu = $page->getMenu();
        for ($i = 0; $i < 20; $i++) {
            $menu->addItem(__("Пункт меню %s", $i), Request::setParam('menu', $i), Request::getParam('menu') == $i);
        }

        $tabs = $page->getTabs();
        for ($i = 0; $i < 4; $i++) {
            $tabs->addItem(__("Вкладка %s", $i), Request::setParam('tab', $i), Request::getParam('tab') == $i);
        }

        $breadcrumbs = $page->getBreadcrumbs();
        for ($i = 0; $i < 4; $i++) {
            $breadcrumbs->add(__("Часть пути %s", $i), '/');
        }

        $content = $page->getContent();
        for ($i = 0; $i < 4; $i++) {
            $post = $content->addPost();
            $post->setUrl(Request::setParam('post', $i));
            $post->setTitle(__("Сообщение %s", $i));
            $post->setContentHtml('Меню:'.Request::getParam('menu', 0).' Вкладка:'.Request::getParam('tab', 0));
            $post->setBottomHtml('bottom');
            $post->setTime(999999999);
            $post->counter = $i * 40;
            $post->highlight = Request::getParam('post', -1) == $i;
        }
    }
}
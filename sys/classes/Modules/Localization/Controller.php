<?php

namespace Modules\Localization;

class Controller extends \Dcms\Core\Controller
{

    /**
     * Определение текущей локали
     */
    function PREPROCESS_locale_detect()
    {
        
    }

    /**
     * Модификация строки в соответствии с локалью
     * @param string $string
     */
    function HOOK_replace_string(&$string)
    {
        
    }

    /**
     * Модификация даты в соответствии с локалью
     * @param array $args
     */
    function HOOK_replace_date(&$args)
    {
        $date = &$args['date'];
        $format = $args['format'];
        $timestamp = $args['timestamp'];
    }
}
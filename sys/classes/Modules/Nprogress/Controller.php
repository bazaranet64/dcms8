<?php

namespace Modules\Nprogress;

use Dcms\Models\Ui\Meta\Meta;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_nprogress(Meta $page_meta)
    {
        $page_meta->scripts[] = $this->getCurrentPathRel().'/files/nprogress.js';
        $page_meta->styles[] = $this->getCurrentPathRel().'/files/nprogress.css';
    }
}
<?php

namespace Modules\Install;

use Dcms\Core\System;
use Dcms\Core\Modules;
use Dcms\Core\Response\Response;
use Dcms\Misc\Json;
use Dcms\Models\Ui\Content\Items\Forms\Form;

class Controller extends \Dcms\Core\Controller
{
    protected
        $_settings,
        $_count_steps = 2,
        $_current_step;

    function __construct($module)
    {
        parent::__construct($module);
        $this->_init();
    }

    protected function _init()
    {
        $settings = $this->getSettings();
        $this->_settings = $settings->get('settings', System::getAllSettings());
        $this->_current_step = $settings->get('current_step', 0);
    }

    /**
     * @param int $step_num
     * @return Form
     */
    function getStepForm($step_num)
    {
        $form = new Form();

        /*
          if ($step_num > 0)
          $form->addButton(__('К началу установки'), 'reset');

          switch ($step_num) {
          case 0:
          $form->addTextField(__('Сервер базы данных'), 'host', $this->_settings['database']['host']);
          $form->addTextField(__('Имя базы данных'), 'dbname', $this->_settings['database']['dbname']);
          $form->addTextField(__('Пользователь'), 'user', $this->_settings['database']['user']);
          $form->addTextField(__('Пароль'), 'password', $this->_settings['database']['password']);
          $form->addButton(__('Подключиться'));
          break;
          case 1:
          $form->addButton(__('Установить'));
          break;
          }
         */
        $form->addButton(__('Установить'));
        return $form;
    }

    /**
     * @param $response Response
     * @throws \Exception
     */
    function GET_install($response)
    {
        if (!class_exists('pdo')) {
            throw new \Exception(__("Отсутствует драйвер PDO"));
        }

        $response->page->setTitle(__('Установка. Шаг %s из %s', $this->_current_step + 1, $this->_count_steps));
        $response->page->content->items[] = $this->getStepForm($this->_current_step);
    }

    function POST_install(Response $response)
    {
        try {
            $form = $this->getStepForm($this->_current_step);

            if ($form->getValue('reset')) {
                $this->getSettings()->clear();
                $this->_init();
                return;
            }

            /*
              switch ($this->_current_step) {
              case 0:
              $host = $form->getValue('host');
              $dbname = $form->getValue('dbname');
              $user = $form->getValue('user');
              $password = $form->getValue('password');

              $database = system::getProperty('database', array());
              $db = new PDO($database['driver'] . ':host=' . $host . ';dbname=' . $dbname, $user, $password);
              $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
              $db->query("SET NAMES utf8;");

              $database['host'] = $host;
              $database['dbname'] = $dbname;
              $database['user'] = $user;
              $database['password'] = $password;

              system::setProperty('database', $database);

              $this->_current_step++;
              break;
              case 1:
              $this->_install();
              break;
              }
             */
            $this->_install($response);
        } catch (\Exception $e) {
            $response->page->err($e->getMessage());
        }
    }

    private function _install(Response $response)
    {
        $modules_file = $this->getCurrentPathAbs() . '/modules_to_install.json';
        if (false === ($modules_json = @file_get_contents($modules_file))) {
            throw new \Exception(__('Не удалось прочитать файл со списком устанавливаемых модулей'));
        }

        try {
            $modules_names = Json::decode($modules_json);
        } catch (\Exception $e) {
            throw new \Exception(__('Не удалось распарсить JSON файл со списком устанавливаемых модулей (%s)'),
                $e->getMessage());
        }

        $dependencies = array();
        $modules = array();
        foreach ($modules_names AS $module_name) {
            $module = Modules::getByName($module_name);

            if (Modules::isInstalled($module)) {
                continue;
            }

            $modules[] = $module;

            $dep = Modules::getModuleDependencies($module);
            foreach ($dep as $dep_module) {
                if (in_array($dep_module->getName(), $dependencies) || in_array($dep_module->getName(), $modules_names)
                ) {
                    continue;
                }
                $dependencies[] = $dep_module->getName();
            }
        }

        foreach ($dependencies as $module_name) {
            $module = Modules::getByName($module_name);
            if (Modules::isInstalled($module)) {
                continue;
            }
            Modules::install($module);
        }

        foreach ($modules as $module) {
            if (Modules::isInstalled($module)) {
                continue;
            }
            Modules::install($module);
        }

        $response->page->msg(__('Все модули успешно установлены'));
    }

    function __destruct()
    {
        $settings = $this->getSettings();
        $settings->set('settings', $this->_settings);
        $settings->set('current_step', $this->_current_step);
        parent::__destruct();
    }
}
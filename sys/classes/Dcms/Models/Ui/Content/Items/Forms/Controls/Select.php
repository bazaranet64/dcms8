<?php

namespace Dcms\Models\Ui\Content\Items\Forms\Controls;

use Dcms\Models\Ui\Content\Items\Forms\Control;

/**
 * Поле select
 * Class Select
 * @property Option[] options
 */
class Select extends Control
{
    public $type    = 'select';
    public $options = array();

    function select($value)
    {
        foreach ($this->options AS $option) {
            $option->selected = $option->value == $value;
        }
    }

    public function getSelectedOption()
    {
        foreach ($this->options AS $option) {
            if ($option->selected) return $option;
        }
    }
}
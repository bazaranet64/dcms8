<?php

namespace Dcms\Models\Ui\Content\Items\Forms\Controls;

use Dcms\Models\Ui\Content\Items\Forms\Control;

/**
 * Элемент поля select
 * Class Option
 */
class Option extends Control
{
    public
        $value    = '',
        $text     = '',
        $selected = false;

}
<?php

namespace Dcms\Models\Ui\Content\Items\Forms;

/**
 * Class Control
 */
abstract class Control
{
    public $type  = null;
    public $title = '';
    public $name  = '';

}
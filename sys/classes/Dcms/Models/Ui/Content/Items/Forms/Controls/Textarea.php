<?php

namespace Dcms\Models\Ui\Content\Items\Forms\Controls;

use Dcms\Models\Ui\Content\Items\Forms\Control;

/**
 * Textarea
 * Class Textarea
 */
class Textarea extends Control
{
    public $type  = 'textarea';
    public $value = '';

    /**
     * Установка значения, которое приходит из вне
     * TODO: запилить паттерны и сверять значение с ними
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
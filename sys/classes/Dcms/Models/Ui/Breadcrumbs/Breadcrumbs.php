<?php
namespace Dcms\Models\Ui\Breadcrumbs;

use Dcms\Core\UiModelRender;

/**
 * Class Breadcrumbs
 * @package Dcms\Models\Ui\Breadcrumbs
 */
class Breadcrumbs extends UiModelRender
{
    public $items = array();

    public function add($text, $url)
    {
        return $this->items[] = new Item($text, $url);
    }
}
<?php

namespace Dcms\Models\Ui\Breadcrumbs;

use Dcms\Core\UiModelRender;

/**
 * Ссылка на страницу в блоке, где указывается путь от главной
 * Class page_breadcrumb
 * @property string url
 * @property string name
 */
class Item extends UiModelRender
{
    public
        $url,
        $name,
        $is_home    = false,
        $is_current = false;

    function __construct($text, $url)
    {
        parent::__construct();
        $this->name = $text;
        $this->url  = $url;
    }
}
<?php

namespace Dcms\Models\Ui\Tabs;

use Dcms\Core\UiModelRender;

/**
 * Блок вкладок
 * Class page_tabs
 * @property Item[] items
 */
class Tabs extends UiModelRender
{
    public $items = array();

    public function addItem($name, $url, $is_active = false)
    {
        $this->items[] = $tab = new Item();
        $tab->setName($name);
        $tab->setUrl($url);
        $tab->is_active = $is_active;
        return $tab;
    }
}
<?php

namespace Dcms\Models\Data\User;

/**
 * Разрешение пользователя (на определенное действие).
 * Class Permission
 */
class Permission
{
    protected $_is_system = false;
    public
        $key           = "",
        $name          = "",
        $description   = "",
        $dependencies  = array();

    function __construct($is_system = false)
    {
        $this->_is_system = $is_system;
    }
}
<?php

namespace Dcms\Models\Data\User;

use Dcms\Core\Db;
use Dcms\Core\Cache;
use Dcms\Misc\Misc;

/**
 * Class User
 * @property Group[] $groups
 */
class User
{
    public
        $id = 0, // публичное свойство нужно для сериализации в JSON
        $login = "";
    protected
        $_id = 0, // защита от дурака в случае изменения публичного свойства
        $_data = array(),
        $_fields_to_update = array();

    /**
     * @param int $id
     * @param bool $use_cache
     */
    function __construct($id = 0, $use_cache = true)
    {
        $this->id = $this->_id = (int) $id;

        if ($this->id === 0) return;

        if (!$use_cache) $this->_getUserDataFromBase();
        else {
            try {
                $this->_getUserDataFromCache();
            } catch (\Exception $e) {
                $this->_getUserDataFromBase();
            }
        }

        $this->_prepareUserData();
    }

    /**
     * @return bool
     */
    public function isAuth()
    {
        return $this->_id !== 0;
    }

    function setPassword($password)
    {
        $new_salt = Misc::getRandomPhrase();
        $this->_setPropertyToSave('password_salt', $new_salt);
        $this->_setPropertyToSave('password_hash', $this->_getPasswordHash($password, $new_salt));
        $this->_saveUserDataToBase(); // такие данные лучше сохранять сразу
    }

    function checkPassword($password)
    {
        return $this->_getPasswordHash($password, $this->_data['password_salt']) === $this->_data['password_hash'];
    }

    protected function _getPasswordHash($password, $salt)
    {
        return md5(md5($password).md5($salt));
    }

    protected function _setPropertyToSave($name, $value)
    {
        if (!array_key_exists($name, $this->_data))
                throw new \Exception(__('В таблице users отсутствует поле "%s"', $name));

        $this->_fields_to_update[] = $name;
        $this->_data[$name] = $value;
        $this->_prepareUserData();
    }

    protected function _getUserDataFromBase()
    {
        $res = Db::me()->prepare("SELECT * FROM `users` WHERE `id` = :id_user LIMIT 1");
        $res->execute(array(':id_user' => $this->_id));
        $data = $res->fetch();
        if (!$data) throw new \Exception(__('Не удалось получить данные пользователя #%s', $this->_id));
        $this->_data = $data;
        $this->_saveUserDataToCache();
    }

    protected function _getUserDataFromCache()
    {
        $data = Cache::get('User.'.$this->_id, false);
        if (!$data) throw new \Exception(__('Не удалось получить данные пользователя #%s', $this->_id));
        $this->_data = $data;
    }

    protected function _saveUserDataToBase()
    {
        if (!$this->_fields_to_update) return;
        $set = array();
        $data = array();
        $this->_fields_to_update = array_unique($this->_fields_to_update);
        foreach ($this->_fields_to_update AS $field) {
            $set[] = '`'.$field.'` = :'.$field;
            $data[':'.$field] = $this->_data[$field];
        }
        $data[':id_user'] = $this->_id;
        //throw new Exception(\Dcms\Misc\Json::encode($data));
        Db::me()->prepare('UPDATE `users` SET '.implode(', ', $set).' WHERE `id` = :id_user LIMIT 1')->execute($data);
        $this->_fields_to_update = array();
        $this->_saveUserDataToCache();
    }

    protected function _saveUserDataToCache()
    {
        Cache::set('User.'.$this->_id, $this->_data, 10);
    }

    protected function _prepareUserData()
    {
        $d = &$this->_data;
        $this->login = $d['login'];
    }

    function __destruct()
    {
        $this->_saveUserDataToBase();
    }
}
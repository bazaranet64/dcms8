<?php

namespace Dcms\Core;

use Dcms\Misc\Json;
use Dcms\Misc\FileSystem;

class Settings
{
    static protected $_settings = array();

    /**
     *
     * @param string $name
     * @param bool $reLoad
     * @return mixed
     */
    static public function getSettings($name, $reLoad = false)
    {
        $filename = basename($name, '.json').'.json';

        if (!array_key_exists($filename, self::$_settings)) {
            self::$_settings[$filename] = null;
        }

        if (self::$_settings[$filename] === null || $reLoad) {
            self::$_settings[$filename] = array();

            if (file_exists(SETTINGS_PATH.'/'.$filename)) {
                $settings_json = @file_get_contents(SETTINGS_PATH.'/'.$filename);
                if ($settings_json) {
                    self::$_settings[$filename] = Json::decode($settings_json);
                }
            } else {
                $settings_json = @file_get_contents(SETTINGS_PATH_DEFAULT.'/'.$filename);
                if ($settings_json) {
                    self::$_settings[$filename] = Json::decode($settings_json);
                }
            }
        }
        return self::$_settings[$filename];
    }

    /**
     *
     * @param string $name
     * @param mixed $settings
     * @throws \Exception
     */
    static public function setSettings($name, $settings = null)
    {
        $filename = basename($name, '.json').'.json';

        if (is_null($settings)) {
            throw new \Exception(__("Не переданы настройки"));
        }

        try {
            FileSystem::fileWrite(SETTINGS_PATH.'/'.$filename, Json::encode($settings));
            self::$_settings[$filename] = $settings;
        } catch (\Exception $e) {
            throw new \Exception(__('Не удалось сохранить настройки модулей (Проверьте права на запись в файл "%s")',
                FileSystem::getRelPath(SETTINGS_PATH.'/'.$filename)));
        }
    }

    /**
     *
     * @param string $name
     */
    public static function clear($name)
    {
        $filename = basename($name, '.json').'.json';
        @unlink(SETTINGS_PATH.'/'.$filename);
    }
}
<?php

namespace Dcms\Core;

use Dcms\Misc\Misc;

/**
 * Базовый класс системы.
 */
abstract class System
{

    /**
     * @return mixed
     */
    static public function getAllSettings()
    {
        return Settings::getSettings('system');
    }

    /**
     * @param string $key
     * @param null|mixed $default
     * @return mixed|null
     */
    static public function getProperty($key, $default = null)
    {
        $settings = Settings::getSettings('system');
        if (!array_key_exists($key, $settings)) {
            return $default;
        }
        return $settings[$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    static public function setProperty($key, $value)
    {
        $settings = Settings::getSettings('system');
        $settings[$key] = $value;
        Settings::setSettings('system', $settings);
    }

    public static function getSalt()
    {
        $salt = self::getProperty('salt', null);
        if (!$salt) {
            $salt = Misc::getRandomPhrase(128);
            self::setProperty('salt', $salt);
        }
        return $salt;
    }

    public static function crypt($encrypt)
    {
        $encrypt = serialize($encrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $key = pack('H*', self::getSalt());
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
        return $encoded;
    }

    public static function decrypt($decrypt)
    {
        $decrypt = explode('|', $decrypt);
        $decoded = base64_decode($decrypt[0]);
        $iv = base64_decode($decrypt[1]);
        if (strlen($iv) !== mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)) {
            return false;
        }
        $key = pack('H*', self::getSalt());
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
        $mac = substr($decrypted, -64);
        $decrypted = substr($decrypted, 0, -64);
        $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
        if ($calcmac !== $mac) {
            return false;
        }
        $decrypted = unserialize($decrypted);
        return $decrypted;
    }
}
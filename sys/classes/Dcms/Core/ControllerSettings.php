<?php

namespace Dcms\Core;

use Dcms\Models\Data\Module;

/**
 * Хранение настроек модуля
 * Все изменение пишутся сразу в файл, потому работает медленнее, чем controller_cache
 * Рекомендуется использовать только для хранения параметров
 * Class controller_settings
 */
class ControllerSettings
{
    protected $_settings_name;
    static $_settings = array();

    /**
     * @param $module Module
     */
    function __construct(Module $module)
    {
        $this->_settings_name = 'module-'.$module->getName();
    }

    /**
     * Получение параметра
     * @param string $name Имя параметра
     * @param mixed $default Значение, если параметр не найден
     * @return bool|mixed
     */
    function get($name, $default = false)
    {
        $settings = Settings::getSettings($this->_settings_name);

        if (!array_key_exists($name, $settings)) return $default;

        return $settings[$name];
    }

    /**
     * Запись параметра
     * @param string $name Имя
     * @param mixed $value Значение
     */
    function set($name, $value)
    {
        $settings = Settings::getSettings($this->_settings_name);
        $settings[$name] = $value;

        Settings::setSettings($this->_settings_name, $settings);
    }

    /**
     * Очистка параметров модуля
     */
    function clear()
    {
        Settings::clear($this->_settings_name);
    }
}
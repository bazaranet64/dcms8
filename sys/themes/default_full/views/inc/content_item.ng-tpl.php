<script type="text/ng-template" id="content_item.html">
    <div ng-if="false" data-ng-repeat-start="data in content"></div>
    <?php include __DIR__ . '/content_item/default.ng-tpl.php'; ?>
    <?php include __DIR__ . '/content_item/html.ng-tpl.php'; ?>
    <?php include __DIR__ . '/content_item/post.ng-tpl.php'; ?>
    <?php include __DIR__ . '/content_item/pagination.ng-tpl.php'; ?>
    <?php include __DIR__ . '/content_item/form.ng-tpl.php'; ?>
    <?php include __DIR__ . '/content_item/chart.ng-tpl.php'; ?>
    <div ng-if="false" data-ng-repeat-end=""></div>
</script>
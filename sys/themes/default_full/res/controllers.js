angular.module('Dcms', ['monospaced.elastic', 'ExperimentsModule', 'chartsExample.directives'])
        .config(function ($httpProvider) {    // [url]http://habrahabr.ru/post/181009/[/url]
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            $httpProvider.defaults.transformRequest = function (data) {
                return angular.isObject(data) && String(data) !== '[object File]' ? $.param(data) : data;
            };
        })
        .config(function ($locationProvider) {
            $locationProvider.html5Mode(true).hashPrefix('!');
        })
        .directive('ngInitial', function () { // инициализация модели по значению в инпуте
            return {
                restrict: 'A',
                controller: [
                    '$scope', '$element', '$attrs', '$parse',
                    function ($scope, $element, $attrs, $parse) {
                        var getter, setter, val;
                        val = $attrs.ngInitial || $attrs.value || $element.val();
                        getter = $parse($attrs.ngModel);
                        setter = getter.assign;
                        setter($scope, val);
                    }
                ]
            };
        })
        .directive('href', function () {
			
            return {
                scope: true,
                controller: function ($scope, $element, $location) {
                    var post = $scope.post = {
                        diff: 5,
                        selection: true,
                        pageX: null,
                        pageY: null,
                        mdown: false,
                        href: "",
                        mousedown: function (event) {
                            post.href = $(event.currentTarget).attr('data-href');
                            post.pageX = event.pageX;
                            post.pageY = event.pageY;
                            post.selection = false;
                            post.mdown = !$(event.target).hasClass('DCMS_phpcode');
                            if (event.which == 2)
                                event.preventDefault();
                        },
                        mouseup: function (event) {
                            if (!post.mdown)
                                return;
                            post.mdown = false;
                            if (post.selection) {
                                $element.removeClass('selection');
                                return;
                            }

                            if ($(event.target).prop("nodeName").toLowerCase() == 'a')
                                return;

                            if (!post.href)
                                return;


                            //if (event.which == 1)
                            //    window.location.href = post.href;
                            if (event.which == 2)
                                window.open(post.href, '_blank');
                            else {
                                $location.url(post.href);
                                $scope.$apply();
                            }

                            event.stopPropagation();
                        },
                        mousemove: function (event) {
                            if (!post.mdown)
                                return;

                            if (post.pageX > event.pageX + post.diff ||
                                    post.pageX < event.pageX - post.diff ||
                                    post.pageY > event.pageY + post.diff ||
                                    post.pageY < event.pageY - post.diff) {
                                post.selection = true;
                                $element.addClass('selection');
                            }
                        }
                    };

                    $element.on({
                        mousedown: post.mousedown,
                        mousemove: post.mousemove,
                        mouseup: post.mouseup
                    });
                }
            }
        })
        .controller('FormCtrl', // контроллер форм. добавляет поддержку отправки AJAX-ом
                ['$rootScope', '$scope', '$element', '$location',
                    function ($rootScope, $scope, $element, $location) {
                        var form = $scope.form = {
                            skip: [],
                            onInputClick: function (event) {
                                //TODO: записывать в skip имена input submit для пропуска при отправке
                            },
                            onSubmit: function (event) {
                                var formNode = event.target;
                                var url = formNode.action;
                                if (!url)
                                    return;
                                event.preventDefault();

                                var postData = {};
                                for (var i = 0; i < formNode.elements.length; i++) {
                                    var el_name = formNode.elements[i].name;
                                    if (!el_name)
                                        continue;
                                    postData[el_name] = formNode.elements[i].value;
                                }

                                $rootScope.$broadcast('formDataSetted', postData);
                                $location.url($(formNode).attr('action'));
                                var oldHash = $location.hash();
                                $location.hash('formSending');
                                $scope.$apply();
                                $location.hash(oldHash);
                            }
                        };

                        $element.on('submit', form.onSubmit);
                    }])
        .controller('DcmsCtrl', // общий контроллер DCMS
                ['$sce', '$scope', '$http', '$timeout', '$rootScope', '$location',
                    function ($sce, $scope, $http, $timeout, $rootScope, $location) {
                        $scope.html = function (html) {
                            return $sce.trustAsHtml(html + "");
                        };
                        angular.extend($scope, {
                            online: true,
                            requesting: false,
                            postData: null,
                            postDataSendTime: 0,
                            StartURL: $location.absUrl(), // адрес текущей страницы
                            DynamicURL: "",
                            model: window.page_data.model || {},
                            model_key: window.page_data.model_key,
                            timeout: null
                        });

                        $scope.requestModel = function (data) {
						
                            // Старт индикатор
                            NProgress.start();
							
                            data = data || {};
                            if ($scope.requesting && !data.force)
                                return;
                            $timeout.cancel($scope.timeout);
                            $scope.requesting = true;

                            var request = function () {
                                if ($scope.postData) {
                                    return $http.post($scope.DynamicURL, $scope.postData, {headers: {'X-DCMS-Model-Key': $scope.model_key}});
                                }
                                else {
                                    return $http.get($scope.DynamicURL, {headers: {'X-DCMS-Model-Key': $scope.model_key}});
                                }
                            };

                            request()
                                    .success(function (data) {
                                        try {
                                            $scope.postData = null;
                                            $scope.online = true;
                                            $scope.requesting = false;
                                            $scope.model_key = data.model_key;
                                            DCMS_CLIENT_MODEL.mergeModel($scope.model, data.model);
                                        } finally {
                                            $scope.timeout = $timeout($scope.requestModel, $scope.model.meta.ajax_timeout * 1000);
                                        }
										
                                    // Конец индикатор
                                    NProgress.done();
                                    
                                    })
                                    .error(function () {
                                        $scope.timeout = $timeout($scope.requestModel, $scope.model.meta.ajax_timeout_error * 1000);
                                        $scope.requesting = false;
                                        $scope.online = false;
                                    });
                        };
                        $timeout($scope.requestModel, $scope.model.meta.ajax_timeout * 1000);

                        $rootScope.$on('formDataSetted', function (event, postData) {
                            $scope.postData = postData;
                            $scope.postDataSendTime = new Date().getTime();
                        });

                        $rootScope.$on('$locationChangeSuccess', function () {
                            if (!$scope.DynamicURL) {
                                $scope.DynamicURL = $location.absUrl();
                                return;
                            }
                            $scope.DynamicURL = $location.absUrl();

                            if (!$scope.postData) {
                                // ограничение на кол-во запросов при изменении адреса в секунду
                                // также предотвращает от отправки GET запроса сразу после POST из за принудительного изменения хэша в адресе
                                if (new Date().getTime() < $scope.postDataSendTime + 1000) {
                                    return;
                                }
                            }

                            $scope.requestModel({
                                force: true
                            });
                        });
                    }
                ])
        ;

